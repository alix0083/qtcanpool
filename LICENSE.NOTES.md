<!-- TOC -->

- [说明](#说明)
- [MulanPSL-2.0](#mulanpsl-20)
- [LGPL-2.1](#lgpl-21)

<!-- /TOC -->

# 说明
qtcanpool 集成了一些第三方组件，这些组件具有自己的许可协议，与 qtcanpool 遵循的许可协议可能存在使用冲突。

本说明文档会以不同的许可协议进行分类，罗列遵循这些许可协议的组件，用户可根据许可协议按需使用相关组件。

# MulanPSL-2.0
- src/libs/qcanpool
- projects/template

```
Copyright (c) [Year] [name of copyright holder]
[Software Name] is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
```

# LGPL-2.1
- src/libs/qads

```
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; If not, see <http://www.gnu.org/licenses/>.
```
