/**
 * Copyleft (C) 2023 maminjie <canpool@163.com>
 * SPDX-License-Identifier: MulanPSL-2.0
 **/
#pragma once

#include "QxGlobal.h"
#include <QIcon>

namespace Qx {

/* color */
QX_WIDGET_EXPORT QIcon colorIcon(const QColor &color, const QSize &size);

} // namespace Qx
