TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS = \
    quickaccessbar \
    tinytabbar \
    tinytabwidget
